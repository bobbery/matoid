import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'globals.dart';

class ChargePage extends StatefulWidget {
  @override
  _ChargePageState createState() => _ChargePageState();
}

class _ChargePageState extends State<ChargePage> {
  int euroSliderValue = 0;
  int centSliderValue = 0;

  int sum = 0;

  List<Widget> theButtons() {
    List<Widget> buttons = [];
    buttons.add(Spacer());
    buttons.add(
      ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: EdgeInsets.all(20), primary: Globals.LightRed),
        child: Text("Okay", style: TextStyle(fontSize: 50)),
        onPressed: () {
          Globals.credit += sum;
          Navigator.of(context).pushReplacement(Globals.switchToMainMenu());
        },
      ),
    );
    buttons.add(Spacer());
    buttons.add(
      ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: EdgeInsets.all(20), primary: Globals.LightRed),
        child: Text("Abbrechen", style: TextStyle(fontSize: 50)),
        onPressed: () {
          Navigator.of(context).pushReplacement(Globals.switchToMainMenu());
        },
      ),
    );
    buttons.add(Spacer());

    return buttons;
  }

  Widget buttonRow() {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center, children: theButtons());
  }

  List<Widget> theSliders() {
    List<Widget> sliders = [];
    sliders.add(Slider(
      value: euroSliderValue.toDouble(),
      min: 0,
      max: 50,
      divisions: 50,
      label: euroSliderValue.toString(),
      onChanged: (double value) {
        setState(() {
          euroSliderValue = value.toInt();
          sum = euroSliderValue * 100 + centSliderValue;
        });
      },
    ));

    sliders.add(Container(height: 50));
    sliders.add(Slider(
        value: centSliderValue.toDouble(),
        min: 0,
        max: 100,
        divisions: 100,
        label: centSliderValue.toString(),
        onChanged: (double value) {
          setState(() {
            centSliderValue = value.toInt();
            sum = euroSliderValue * 100 + centSliderValue;
          });
        }));
    return sliders;
  }

  Widget sliders() {
    return Column(children: theSliders());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          backgroundColor: Colors.green[200],
          body: Column(
            children: [
              Spacer(),
              place(
                Text("Aufladen: ${(sum / 100.0).toStringAsFixed(2)} €",
                    style: TextStyle(fontSize: 50)),
                Alignment.topCenter,
              ),
              Spacer(),
              place(sliders(), Alignment.center),
              Spacer(),
              place(buttonRow(), Alignment.bottomCenter),
              Spacer(),
            ],
          ),
        ));
  }

  Widget place(Widget child, Alignment align) => Align(
        alignment: align,
        child: child,
      );
}
