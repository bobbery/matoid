import 'package:flutter/material.dart';
import 'package:matoid/data.dart';
import 'package:wakelock/wakelock.dart';
import 'package:flutter/services.dart';
import 'package:screen/screen.dart';

import 'globals.dart';
import 'startanim.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return MaterialApp(
      title: Globals.title,
            theme: ThemeData(
          primarySwatch: Colors.red,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          fontFamily: 'Mulish'),
      home: ScreenSaverPage(),
    );
  }
}

class ScreenSaverPage extends StatefulWidget {
  ScreenSaverPage();
  final String title = Globals.title;

  @override
  _ScreenSaverPageState createState() => _ScreenSaverPageState();
}

class _ScreenSaverPageState extends State<ScreenSaverPage> {
  FocusNode myFocusNode = FocusNode();
  String _input = "";

  @override
  void initState() {
    super.initState();
    Wakelock.enable();
    Screen.keepOn(true);
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    super.dispose();
  }

  void _onRawKeyEvent(RawKeyEvent event) {
    if (event.character != '\n') {
      _input = _input + event.character;
    } else {
      Globals.token = _input;
      fetchPrice().then((value) {
        getCredit(Globals.token).then((value) {
          Globals.credit = value;

          if (Globals.token == Globals.matminToken) {
            Navigator.of(context).pushReplacement(Globals.switchToMatminPage());
          } else {
            Navigator.of(context).pushReplacement(Globals.switchToMainMenu());
          }
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Material(
        child: WillPopScope(
            onWillPop: () async => false,
            child: Center(
              child: Stack(children: [
                Opacity(
                  opacity: 0.0,
                  child: Container(
                    child: RawKeyboardListener(
                      focusNode: myFocusNode,
                      autofocus: true,
                      onKey: _onRawKeyEvent,
                      child: Text("Input"),
                    ),
                  ),
                ),
                Stack(children: <Widget>[
                  Positioned.fill(child: AnimatedBackground()),
                  Positioned.fill(child: Particles(30)),
                  Positioned.fill(child: CenteredImageFader()),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      "STS",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: InkWell(
                        onDoubleTap: () {
                          // Globals.token = "0000";
                          // fetchPrice().then((value) {
                          //   getCredit(Globals.token).then((value) {
                          //     Globals.credit = value;
                          // Navigator.of(context)
                          //     .pushReplacement(Globals.switchToMainMenu());
                          //   });
                          // });
                        },
                        child: Image.asset(
                          'assets/rfid.png',
                          width: 128,
                        )),
                  ),
                ]),
              ]),
            )));
  }
}
