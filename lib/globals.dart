import 'package:flutter/material.dart';
import 'main.dart';
import 'mainmenu.dart';
import 'charge.dart';
import 'exit.dart';
import 'matmin.dart';

class Globals {

  static final Color LightRed = Color.fromARGB(255, 187, 30, 16);

  static final title = "Mat-O-Id";

  static String token = "123";

  static int credit = 1000;
  static int price = 70;

  static final String matminToken = "2846299786";

  static Route switchToScreenSaver() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          ScreenSaverPage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, -1.0);
        var end = Offset.zero;
        var tween = Tween(begin: begin, end: end);
        var offsetAnimation = animation.drive(tween);

        return SlideTransition(
          position: offsetAnimation,
          child: child,
        );
      },
    );
  }

  static Route switchToChargePage() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => ChargePage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, -1.0);
        var end = Offset.zero;
        var tween = Tween(begin: begin, end: end);
        var offsetAnimation = animation.drive(tween);

        return SlideTransition(
          position: offsetAnimation,
          child: child,
        );
      },
    );
  }

  static Route switchToMainMenu() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => MainMenuPage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var tween = Tween(begin: begin, end: end);
        var offsetAnimation = animation.drive(tween);

        return SlideTransition(
          position: offsetAnimation,
          child: child,
        );
      },
    );
  }

  static Route switchToExitPage() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => ExitPage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var tween = Tween(begin: begin, end: end);
        var offsetAnimation = animation.drive(tween);

        return SlideTransition(
          position: offsetAnimation,
          child: child,
        );
      },
    );
  }

  static Route switchToMatminPage() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => MatminPage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var tween = Tween(begin: begin, end: end);
        var offsetAnimation = animation.drive(tween);

        return SlideTransition(
          position: offsetAnimation,
          child: child,
        );
      },
    );
  }
}
