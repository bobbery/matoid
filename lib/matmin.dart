import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'globals.dart';
import 'data.dart';

class MatminPage extends StatefulWidget {
  @override
  _MatminPageState createState() => _MatminPageState();
}

class _MatminPageState extends State<MatminPage> {
  int centSliderValue = 0;
  int allCredit = 0;

  @override
  void initState() {
    super.initState();

    fetchPrice().then((value) => setState(() {
          centSliderValue = Globals.price;
        }));

    getAllCredit().then((value) {
      setState(() {
        allCredit = value;
      });
    });
  }

  List<Widget> theButtons() {
    List<Widget> buttons = [];
    buttons.add(Spacer());
    buttons.add(
      ElevatedButton(
        style: ElevatedButton.styleFrom(padding: EdgeInsets.all(20)),
        child: Text("Okay", style: TextStyle(fontSize: 50)),
        onPressed: () {
          savePrice(centSliderValue);
          Navigator.of(context).pushReplacement(Globals.switchToScreenSaver());
        },
      ),
    );
    buttons.add(Spacer());
    buttons.add(
      ElevatedButton(
        style: ElevatedButton.styleFrom(padding: EdgeInsets.all(20)),
        child: Text("Abbrechen", style: TextStyle(fontSize: 50)),
        onPressed: () {
          Navigator.of(context).pushReplacement(Globals.switchToScreenSaver());
        },
      ),
    );
    buttons.add(Spacer());

    return buttons;
  }

  Widget buttonRow() {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center, children: theButtons());
  }

  List<Widget> theSliders() {
    List<Widget> sliders = [];

    sliders.add(Container(height: 50));
    sliders.add(Slider(
        value: centSliderValue.toDouble(),
        min: 0,
        max: 100,
        divisions: 100,
        label: centSliderValue.toString(),
        onChanged: (double value) {
          setState(() {
            centSliderValue = value.toInt();
          });
        }));
    return sliders;
  }

  Widget sliders() {
    return Column(children: theSliders());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          backgroundColor: Colors.redAccent,
          body: Column(
            children: [
              Spacer(),
              place(
                  Column(
                    children: [
                      Text(
                          "Gesamt: ${(allCredit / 100.0).toStringAsFixed(2)} €",
                          style: TextStyle(fontSize: 50)),
                      Text(
                          "Flaschenpreis: ${(centSliderValue / 100.0).toStringAsFixed(2)} €",
                          style: TextStyle(fontSize: 50)),
                    ],
                  ),
                  Alignment.topCenter),
              Spacer(),
              place(sliders(), Alignment.center),
              Spacer(),
              place(buttonRow(), Alignment.bottomCenter),
              Spacer(),
            ],
          ),
        ));
  }

  Widget place(Widget child, Alignment align) => Align(
        alignment: align,
        child: child,
      );
}
