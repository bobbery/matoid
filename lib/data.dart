import 'package:sqflite/sqflite.dart';
import 'globals.dart';

Future<void> saveCredit(String token, int credit) async {
  Database _db = await openDatabase('matoid.db', version: 1,
      onCreate: (Database db, int version) async {
    await db.execute('CREATE TABLE tokenCredit ( '
        'token TEXT PRIMARY KEY, '
        'credit INT)');
    await db.execute('CREATE TABLE price (price INT)');
  });
  var vals1 = Map<String, Object>();
  vals1['token'] = token;
  vals1['credit'] = credit;
  await _db.insert('tokenCredit', vals1,
      conflictAlgorithm: ConflictAlgorithm.replace);
  _db.close();
}

Future<void> savePrice(int price) async {
  Database _db = await openDatabase('matoid.db', version: 1,
      onCreate: (Database db, int version) async {
    await db.execute('CREATE TABLE tokenCredit ( '
        'token TEXT PRIMARY KEY, '
        'credit INT)');
    await db.execute('CREATE TABLE price (price INT)');
  });
  var vals1 = Map<String, Object>();
  vals1['price'] = price;
  await _db.delete('price');
  await _db.insert('price', vals1,
      conflictAlgorithm: ConflictAlgorithm.replace);
  _db.close();
}

Future<int> getCredit(String token) async {
  Database _db = await openDatabase('matoid.db', version: 1,
      onCreate: (Database db, int version) async {
    await db.execute('CREATE TABLE tokenCredit ( '
        'token TEXT PRIMARY KEY, '
        'credit INT)');
    await db.execute('CREATE TABLE price (price INT)');
  });
  var result =
      await _db.rawQuery("SELECT credit FROM tokenCredit WHERE token='$token'");
  _db.close();
  if (result.length > 0) {
    return result[0]['credit'];
  }
  return 0;
}

Future<int> getAllCredit() async {
  Database _db = await openDatabase('matoid.db', version: 1,
      onCreate: (Database db, int version) async {
    await db.execute('CREATE TABLE tokenCredit ( '
        'token TEXT PRIMARY KEY, '
        'credit INT)');
    await db.execute('CREATE TABLE price (price INT)');
  });
  var result = await _db.rawQuery("SELECT SUM(credit) FROM tokenCredit");
  _db.close();
  if (result.length > 0) {
    return result[0]['SUM(credit)'];
  }
  return 0;
}

Future<void> fetchPrice() async {
  Database _db = await openDatabase('matoid.db', version: 1,
      onCreate: (Database db, int version) async {
    await db.execute('CREATE TABLE tokenCredit ( '
        'token TEXT PRIMARY KEY, '
        'credit INT)');
    await db.execute('CREATE TABLE price (price INT)');
  });
  var result = await _db.rawQuery("SELECT price FROM price");
  _db.close();
  if (result.length > 0) {
    Globals.price = result[0]['price'];
  } else {
    Globals.price = 70;
  }
}
