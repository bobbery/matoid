import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'globals.dart';
import 'data.dart';

class ExitPage extends StatefulWidget {
  @override
  _ExitPageState createState() => _ExitPageState();
}

class _ExitPageState extends State<ExitPage> {
  List<Widget> theButtons() {
    List<Widget> buttons = [];
    buttons.add(Spacer());
    buttons.add(
      ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: EdgeInsets.all(20), primary: Globals.LightRed),
        child: Text("Zurück", style: TextStyle(fontSize: 50)),
        onPressed: () {
          Navigator.of(context).pushReplacement(Globals.switchToMainMenu());
        },
      ),
    );
    buttons.add(Spacer());
    buttons.add(
      ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: EdgeInsets.all(20), primary: Globals.LightRed),
        child: Text("Ende", style: TextStyle(fontSize: 50)),
        onPressed: () {
          saveCredit(Globals.token, Globals.credit).then((value) {
            Navigator.of(context)
                .pushReplacement(Globals.switchToScreenSaver());
          });
        },
      ),
    );
    buttons.add(Spacer());

    return buttons;
  }

  Widget buttonRow() {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center, children: theButtons());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          backgroundColor: Colors.yellow,
          body: Column(
            children: [
              Spacer(),
              place(
                Text("Entnahme\nerfolgreich", style: TextStyle(fontSize: 50)),
                Alignment.topCenter,
              ),
              Spacer(),
              place(buttonRow(), Alignment.bottomCenter),
              Spacer(),
            ],
          ),
        ));
  }

  Widget place(Widget child, Alignment align) => Align(
        alignment: align,
        child: child,
      );
}
