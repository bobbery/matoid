import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'globals.dart';
import 'mainmenuanim.dart';
import 'data.dart';

class MainMenuPage extends StatefulWidget {
  @override
  _MainMenuPageState createState() => _MainMenuPageState();
}

class _MainMenuPageState extends State<MainMenuPage> {

  @override
  void initState() {
    super.initState();

    fetchPrice().then((value) => setState(() {}));
  }

  List<Widget> theBottles(int numBottles) {
    List<Widget> bottles = [];
    for (int i = 0; i < numBottles; i++) {
      bottles.add(SizedBox(
          width: 30, height: 100, child: Image.asset('assets/bottle.png')));
    }

    return bottles;
  }

  Widget bottleRow(int numBottles) {
    return Row(children: theBottles(numBottles));
  }

  List<Widget> theButtons() {
    List<Widget> buttons = [];
    buttons.add(Spacer());
    buttons.add(
      ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: EdgeInsets.all(20), primary: Globals.LightRed),
        child: Text("Entnahme", style: TextStyle(fontSize: 50, color: Colors.white)),
        onPressed: () {
          if (Globals.credit >= Globals.price) {
            print("Entnahme");
            Globals.credit -= Globals.price;
            Navigator.of(context).pushReplacement(Globals.switchToExitPage());
          }
        },
      ),
    );
    buttons.add(Spacer());
    buttons.add(
      ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: EdgeInsets.all(20), primary: Globals.LightRed),
        child: Text("Aufladen", style: TextStyle(fontSize: 50, color: Colors.white)),
        onPressed: () {
          Navigator.of(context).pushReplacement(Globals.switchToChargePage());
        },
      ),
    );
    buttons.add(Spacer());
    buttons.add(
      ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: EdgeInsets.all(20), primary: Globals.LightRed),
        child: Text("Ende", style: TextStyle(fontSize: 50, color: Colors.white)),
        onPressed: () {
          saveCredit(Globals.token, Globals.credit).then((value) {
            Navigator.of(context)
                .pushReplacement(Globals.switchToScreenSaver());
          });
        },
      ),
    );
    buttons.add(Spacer());

    return buttons;
  }

  Widget buttonRow() {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center, children: theButtons());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          body: Stack(
            children: [
              onBottom(AnimatedWave(
                180,
                1.0,
                0,
              )),
              onBottom(AnimatedWave(
                120,
                0.9,
                pi,
              )),
              onBottom(AnimatedWave(
                220,
                1.2,
                pi / 2,
              )),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 120,
                  child: Flex(direction: Axis.horizontal, children: [Image.asset('assets/banner.png')], ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 120,
                  child: Image.asset('assets/frame3.png'),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Column(
                  children: [
                    Text("Mat-O-Id", style: TextStyle(color: Colors.white, fontSize: 50)),
                    Text(
                        "Guthaben: ${(Globals.credit / 100.0).toStringAsFixed(2)} €",
                        style: TextStyle(fontSize: 36, color: Colors.white)),
                    Container(height: 20),
                    bottleRow((Globals.credit ~/ Globals.price).toInt()),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: buttonRow(),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  "Flaschenpreis ${(Globals.price / 100.0).toStringAsFixed(2)} €",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ));
  }

  Widget onBottom(Widget child) => Positioned.fill(
        child: Align(
          alignment: Alignment.bottomCenter,
          child: child,
        ),
      );
}
